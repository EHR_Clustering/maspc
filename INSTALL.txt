COMPILATION
------------------------
We used python (3.7.1) and it was tested on the following operating systems (Windows 10, 17134.915)

INFORMATION ABOUT THE INPUT AND OUTPUT
---------------------------------------
There are 4 parameters for MASPC: minSup, minAc, minO and k
Please use instanceName. ClusterResult to check clustering results

For PC, method='average' and metric='cosine' are parameters for agglomerative average-linkage hierarchical clustering. 

HOW TO RUN MASPC
----------------------------------------
if __name__ == "__main__":
    maspc = MASPC() # create an instance 
    maspc.MAS(minSup=*,minAc=*,minOv=*) # run MAS
    maspc.PC(k=*,method='average',metric='cosine') # run PC


Comments and Questions
----------------------
Haodi Zhong
Haodi.zhong@kcl.ac.uk

Grigorios Loukides
grigorios.loukides@kcl.ac.uk


