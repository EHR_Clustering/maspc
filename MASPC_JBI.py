import numpy as np
import pandas as pd
import scipy
import subprocess
from sklearn import metrics
import csv
from scipy.cluster.hierarchy import fcluster
from scipy.cluster.hierarchy import linkage

# Data input

# We use a small data to demonstrate a clustering algorithm, MASPC, which is published in Journal of Biomedical Informatics
# In this small data, we assume preprocess step has done for Demographic, which means numerical attributes in demographic will be discretized
# users can any discretization algorithms at here
# Then categorical demographic attributes and discretized numerical attributes will be encoded by one-hot encoding

# The features in the binary representation are: F and M, the values in the categorical demographic attribute Gender
#{45-49},{70-74}, and {75-80}, the values of the discretized numerical attribute Age
demographic = pd.DataFrame(np.array([[1, 0, 0, 0, 1], [0, 1, 0, 1, 0], [1, 0, 1, 0, 0],
                                    [1, 0, 0, 0, 1],[0, 1, 0, 1, 0],[1, 0, 1, 0, 0],
                                    [1, 0, 0, 0, 1],[0, 1, 0, 1, 0],[1, 0, 1, 0, 0],
                                    [1, 0, 0, 0, 1],[0, 1, 0, 1, 0],[1, 0, 1, 0, 0]]),columns=['F', 'M', '45-49','70-74','75-80'])

# Read dignosisCodes.txt as input for dignosis codes
dignosis = open('dignosisCodes.txt', 'r')
dignosisCodes = [line[:-2].split(' ') for line in dignosis.readlines()]

# Check dignosis codes
# Each row is dignosis codes of a patient 
print ('all dignosis codes')
for i in dignosisCodes:
    print(i)


# Define Apriori algorithm
# The Apriori algorithm used at here is built upon SPMF (http://www.philippe-fournier-viger.com/spmf/) 
# Please download spmf.jar from its website before you run Apriori algorithm
# Reference of Apriori: https://en.wikipedia.org/wiki/Apriori_algorithm
# Input of Apriori is self._input = "***.txt", which includes each patients' diagnosis codes
# Output of Apriori is self._output = "***.txt"


class Apriori():
    
    def __init__(self):
        self._executable = "spmf.jar"
        self._input = "dignosisCodes.txt"
        self._output = "Apriori_output.txt"

    def run(self, min_supp):
        subprocess.call(["java", "-Xmx512m", "-jar", self._executable, "run", "Apriori", self._input, self._output, str(min_supp)])

    def encode_input(self, data):
        pass

    def decode_output(self):
        # read
        lines = []
        try:
            with open(self._output, "rU") as f:
                lines = f.readlines()
        except:
            print ("read_output error")

        # decode
        patterns = []
        for line in lines:
            line = line.strip()
            patterns.append(line.split())

        return patterns


# Define FPMax algorithm
# FPMax Algorithm can return Frequent Maximal Itemsets  
# Reference of FPMax Algorithm: Grahne, G., & Zhu, J. (2003, May). High performance mining of maximal frequent itemsets. 
# Input of FPMax is self._input = "***.txt", which includes each patients' diagnosis codes
# Output of FPMax is self._output = "***.txt"

class FPMax():

    def __init__(self):        
        self._executable = "spmf.jar"
        self._input = "dignosisCodes.txt"
        self._output = "FPMax_output.txt"

    def run(self, min_supp):
        subprocess.call(["java", "-Xmx512m", "-jar", self._executable, "run", "FPMax", self._input, self._output, str(min_supp)])

    def encode_input(self, data):
        pass

    def decode_output(self):
        # read
        lines = []
        try:
            with open(self._output, "rU") as f:
                lines = f.readlines()
        except:
            print ("read_output error")

        # decode
        patterns = []
        for line in lines:
            line = line.strip()
            patterns.append(line.split())

        return patterns


def allconfidence(list_1,list_max):
    # Compute All_confidence of an itemset
    b=[]
    for i in list_max[:len(list_max)-2]:
        for j in list_1:
            if i==j[0]:
                b.append(int(j[2]))
    return int(list_max[-1])/max(b)


def get_all_allconfidence(list_1,list_all_max,threshhold):
    # Input a list of MFIs
    # Return MFIs whose All_confidence is above minAc
    all_max=[]
    for i in list_all_max:
        if allconfidence(list_1,i) >=threshhold:
            i[-1]=allconfidence(list_1,i)
            all_max.append(i)
    return all_max

# MASPC algorithm
class MASPC():
    
    def MAS(self,minSup,minAc,minOv):
        # Run FRMax to get MFI 

        fpmax = FPMax()
        fpmax.encode_input([])
        fpmax.run(minSup)
        
        # Running Apriori is a preparatory step for getting MFA

        apriori = Apriori()
        apriori.encode_input([])
        apriori.run(minSup)
        
        
        list_1 = []
        for i in apriori.decode_output():
            if len(i)==3:
                list_1.append(i)
        # Get MFA
        all_con=get_all_allconfidence(list_1,fpmax.decode_output(),minAc)
        all_con.sort(key=lambda x: x[-1],reverse=True)

        all_con_withoutSUP=[]
        for i in all_con:
            all_con_withoutSUP.append([x for x in i[:len(i)-2]])
        
        all_con_target = []
        for i in all_con_withoutSUP:  
            flag = 0
            for j in all_con_target:
                if (set(i) & set(j) != set()):
                    number = 0
                    for k in dignosisCodes:  
                        if ( ( set(k) & (set(i)|set(j)) ) == (set(i)|set(j)) ): 
                            number = number + 1  
                    if number <= minOv:  
                        flag = 1
                        break
            if flag == 0:      
                all_con_target.append(i)
                
        all_con_target_without1=[]

        for i in all_con_target:
            if len(i) != 1:
                all_con_target_without1.append(i)
        # save MFAs
        self.MFAs = all_con_target_without1




class MASPC(MASPC):
    def PC(self,k,method,metric):
        w, h = len(self.MFAs), len(dignosisCodes);
        all_con_tables_without1=[[0 for x in range(w)] for y in range(h)] 

        # project maximum set of independent frequnet patterns 
        for i,j in enumerate(dignosisCodes):
            temp=set(j)
            
            l=len(temp)
            for a,b in enumerate(self.MFAs): 
                while(set(b)<=temp):
                    temp=temp.difference(set(b))
                    
                    all_con_tables_without1[i][a]+=1    

        # build a dataframe
        all_con_part_2_without1=pd.DataFrame(all_con_tables_without1, columns=[str(sublist) for sublist in self.MFAs])    
        all_con_final_t_without1=demographic.join(all_con_part_2_without1)
        # delete the data that not be subscribed
        all_con_delete_without1= [sum(i) for i in all_con_tables_without1]
        all_con_delete_idex_without1=[i for i, e in enumerate(all_con_delete_without1) if e == 0]
        all_con_final_t_without1.drop(all_con_delete_idex_without1,inplace=True)
        self.binaryData=all_con_final_t_without1
        # do clustering
        all_con_cos_ave_without1 = linkage(all_con_final_t_without1.values, method, metric)
        self.ClusterResult=fcluster(all_con_cos_ave_without1, k, criterion='maxclust')




# Run MASPC
# Input parameters: minSup=0.33, minAc=0.5, minOv=3, k=3
# method='average' and metric='cosine' are parameters for agglomerative average-linkage hierarchical clustering

if __name__ == "__main__":
    maspc = MASPC()
    maspc.MAS(minSup=0.33,minAc=0.5,minOv=3)
    maspc.PC(k=3,method='average',metric='cosine')

# Check results of MASPC
# Check MFAs
print(maspc.MFAs)

# Check clustering results
print(maspc.ClusterResult)

# Add label to binary representation
maspc.binaryData['label']=maspc.ClusterResult

# Cluster 1
print('Cluster 1:')
print(maspc.binaryData.groupby(['label']).get_group(1))
# Cluster 2
print('Cluster 2:')
print(maspc.binaryData.groupby(['label']).get_group(2)) 
# Cluster 3
print('Cluster 3:')
print(maspc.binaryData.groupby(['label']).get_group(3)) 


# SI and CI

# Get all unique diagnosis codes and build a binary representation for evaluation
allUniqueCodes=[]
for i in dignosisCodes:
    for j in i:
        allUniqueCodes.append(j)
allUniqueCodes=list(set(allUniqueCodes))
new_list = [allUniqueCodes[i:i+1] for i in range(0, len(allUniqueCodes), 1)]


w, h = len(allUniqueCodes), len(dignosisCodes);
atables=[[0 for x in range(w)] for y in range(h)] 

# project maximum set of independent frequnet patterns 
for i,j in enumerate(dignosisCodes):
    temp=set(j)
    #print temp
    for a,b in enumerate(new_list): 
        while(set(b)<=temp):
            temp=temp.difference(set(b))
            #print temp
            atables[i][a]+=1  

diga_codes=pd.DataFrame(atables, columns=[str(sublist) for sublist in allUniqueCodes])

# Binary representation for evaluation
testdata=pd.concat([demographic, diga_codes], axis=1, sort=False)

# CI and SI
print('CI: ' , metrics.calinski_harabaz_score(testdata.values, maspc.ClusterResult.tolist()))
print('SI: ' ,metrics.silhouette_score(testdata.values, maspc.ClusterResult.tolist(), metric='cosine'))




















