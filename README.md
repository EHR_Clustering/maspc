The folder contains the source code of the algorithms (MASPC) presented in the paper:

Clustering datasets with demographics and diagnosis codes by Zhong et al. (Journal of Biomedical Informatics).

To compile the source code please follow the instructions in file INSTALL.

For convenience, we have included the datasets we used. The datasets are publicly available at:

https://www.healthvermont.gov/health-statistics-vital-records/health-care-systems-reporting/hospital-discharge-data
https://sites.google.com/site/informsdataminingcontest/data/